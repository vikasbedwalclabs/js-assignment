
var Salary = 1000;
var emp = ["EId", "Name", "Company"];
document.getElementById("array").innerHTML = emp[0];



//Access all         
document.getElementById("accessAll").innerHTML = emp;


//Objects use names to access its "members
var emp = {FirstName: "David", LastName: "Hussy", Age: 24};
document.getElementById("accessByName").innerHTML = emp["FirstName"];


//Array Properties and Methods
var emp = ["EId", "Name", "Company"];
document.getElementById("arrayLength").innerHTML = emp.length;


//sorting
document.getElementById("arraySort").innerHTML = emp.sort();

//add a new element to an array is to use the length property
var emp = ["EId", "Name", "Company"];
emp[emp.length] = Salary;

document.getElementById("arrayEleAdd").innerHTML = emp.length;
function myFunctionLoop() {
    var index;
    var text = "<ul>";
    var fruits = ["Banana", "Orange", "Apple", "Mango"];
    for (index = 0; index < fruits.length; index++) {
        text += "<li>" + fruits[index] + "</li>";
    }
    text += "</ul>";
    document.getElementById("arrayLoop").innerHTML = text;
}


//See which type of array we are using
var emp = ["EId", "Name", "Company"];
document.getElementById("arrayType").innerHTML = typeof emp;




//Use of valueOf Function

document.getElementById("arrayValue").innerHTML = emp.valueOf();



//Use of Join. It same as concatenation but we can define separation element
var emp = ["EId", "Name", "Company"];
document.getElementById("arrayJoins").innerHTML = emp.join(" * ");

//pop
var fruits = ["Banana", "Orange", "Apple", "Mango"];
document.getElementById("arrayPop").innerHTML = fruits;

function myFunctionPop() {
    fruits.pop();
    document.getElementById("arrayPop").innerHTML = fruits;
}


//push
var fruits = ["Banana", "Orange", "Apple", "Mango"];
document.getElementById("arrayPush").innerHTML = fruits;

function myFunctionPush() {
    fruits.push("Grapes");
    document.getElementById("arrayPush").innerHTML = fruits;
}



//Shift
var fruits = ["Banana", "Orange", "Apple", "Mango"];
document.getElementById("arrayShift").innerHTML = fruits;

function myFunctionShift() {
    fruits.shift();
    document.getElementById("arrayShift").innerHTML = fruits;
}
var fruits = ["Banana", "Orange", "Apple", "Mango"];
document.getElementById("arrayUnshift").innerHTML = fruits;




//Unshift
function myFunctionUnshift() {
    fruits.unshift("Coconut");
    document.getElementById("arrayUnshift").innerHTML = fruits;
}




//Splice added element to array
var fruits = ["Banana", "Orange", "Apple", "Mango"];
document.getElementById("arraySplice").innerHTML = fruits;
function myFunctionSplice() {
    fruits.splice(1, 0, "Lychee", "Papaya");
    document.getElementById("arraySplice").innerHTML = fruits;
}
var fruits = ["Banana", "Orange", "Apple", "Mango"];
document.getElementById("arrayREvrse").innerHTML = fruits;




//Reverse the element of Array
function myFunctionReverse() {
    fruits.sort();
    fruits.reverse();
    document.getElementById("arrayReverse").innerHTML = fruits;
}





